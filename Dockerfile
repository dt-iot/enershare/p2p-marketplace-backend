# Build
FROM node:14

# Create the working directory inside the container
RUN mkdir /app

# Set the working directory inside the container
WORKDIR /app

# Copy package.json e package-lock.json files into the working directory

COPY package*.json ./

# Install dependencies
RUN npm install

RUN npm i bcrypt

# Copy the rest of your application files
COPY . .

# Expose the port your app runs on
EXPOSE 8080

# Define the command to run your app
CMD ["node", "server.js"]
