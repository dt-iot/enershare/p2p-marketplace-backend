# DSO Platform - Backend

![REPO-TYPE](https://img.shields.io/badge/repo--type-backend-success?style=for-the-badge&logo=node.js)

The DSO Platform is a software component developed within the ENERSHARE project.

The DSO Plaftorm backend takes care of retrieving the data, necessary for the user's graphical interface (GUI), through various external service APIs, ensuring that the user can view and interact with data effectively and dynamically.

Furthermore, the system manages the interaction between the user and the database, allowing fundamental operations such as registration and account access.
Users can register by creating a new account via an e-mail and password, which are then securely saved within the database.
 
Graphical User Interface (GUI) Link:
https://gitlab.com/dt-iot/enershare/dso-platform-frontend

## Auth - Signup & Login with Email (JWT) | Node, Express, MongoDB

Authentication is implemented using Node.js, Express & MongoDB. User can signup and login to there account with their email.
For authentication purposes, json web token (JWT) is used.

## Built With

- [Node](https://nodejs.org/en)
- [MongoDB](https://www.mongodb.com/it-it)

## Configuring

Here is the list of the environment variables configurable in the .env file (for the standard installation) and in the Dockerfile and the Docker compose file (for the docker installation) in the root of the project.

| VARIABLE NAME                  | DESCRIPTION                                                       | DEFAULT VALUE                                |
| ------------------------------ | ----------------------------------------------------------------- | -------------------------------------------- |
| **DB**      | The base url of the MongoDB instance                     | `mongodb://localhost:27017/IotMonitoring`                       |

&nbsp;

## Getting Started

### Prerequisites

* Node 20+

### Installing

- Check out the code from this repository:

```
 git clone https://gitlab.com/dt-iot/enershare/dso-platform-backend.git
```

- Move into the root directory of the application:

```
cd p2p-marketplace-backend
```

- Run the following command:

```
npm install
```

### Running

In the project directory, run the following command:

```
node server.js
```
&nbsp;

## Getting Started Using Docker

### Prerequisites

- [Docker](https://docs.docker.com/get-started/)

### Installing

Check out the code from this repository:

```
git clone https://gitlab.com/dt-iot/enershare/dso-platform-backend.git
```

Move into the root directory of the application:

```
cd p2p-marketplace-backend
```

### Running

Run the following command:

```
docker compose up -d
```